# remind

little go cli tool to remind you of things

## installation

```bash
go get git.envs.net/lel/remind
```

## usage

To add a reminder:
```bash
$ remind -add physics 'do physics homework'
```

To list reminders:
```bash
$ remind -list
	[physics] 	do physics homework
```

To show the contents of a specific reminder (idk why this might be useful but i did it anyway):
```bash
$ remind -show physics
do physics homework
```

To display a random reminder (*this is the primary purpose -- for putting in bar shell scripts and stuff*)
```bash
$ remind
do physics homework
```

To clear reminders:
```bash
$ remind -clear
```