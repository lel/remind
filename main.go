package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

func getFilename(filename string) (string, error) {
	if filename == "" {
		filename = path.Join(os.Getenv("HOME"), ".local/share/remind/remind.json")
	}
	folder := filepath.Dir(filename)
	if _, err := os.Stat(folder); err != nil {
		err := os.MkdirAll(folder, os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return filename, nil
}

func main() {
	flagAdd := flag.String("add", "", "add a new reminder")
	flagRemove := flag.String("remove", "", "remove a reminder")
	flagList := flag.Bool("list", false, "list reminders")
	flagFile := flag.String("file", "", "change reminder file location")
	flagShow := flag.String("show", "", "examine a particular reminder")
	flagClear := flag.Bool("clear", false, "clear all reminders")
	log.SetPrefix(fmt.Sprintf("%s: ", os.Args[0]))
	log.SetFlags(0)
	flag.Parse()
	filename, err := getFilename(*flagFile)
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reminders, err := getReminders(file)
	if err != nil {
		log.Fatal(err)
	}
	if *flagClear {
		reminders = make(map[string]string)
		save(file, reminders)
		return
	}
	if *flagAdd != "" {
		if flag.NArg() < 1 {
			flag.Usage()
			os.Exit(2)
		}
		reminders[*flagAdd] = strings.Join(flag.Args(), " ")
		err := save(file, reminders)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagRemove != "" {
		if flag.NArg() != 0 {
			flag.Usage()
			os.Exit(2)
		}
		_, err := getReminder(reminders, *flagRemove)
		if err != nil {
			log.Fatal(err)
		}
		delete(reminders, *flagRemove)
		err = save(file, reminders)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagList {
		err := list(reminders)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagShow != "" {
		if flag.NArg() != 0 {
			flag.Usage()
			os.Exit(2)
		}
		val, err := getReminder(reminders, *flagShow)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(val)
		return
	}
	if flag.NArg() > 0 {
		flag.Usage()
		os.Exit(2)
	}
	if len(reminders) < 1 {
		log.Fatal(errors.New("no reminders"))
	}
	vals := []string{}
	for _, val := range reminders {
		vals = append(vals, val)
	}
	rand.Seed(time.Now().UnixNano())
	fmt.Println(vals[rand.Intn(len(vals))])
	return
}

func getReminder(reminders map[string]string, name string) (string, error) {
	if val, ok := reminders[name]; ok {
		return val, nil
	} else {
		return "", errors.New("no such reminder")
	}
}

func list(reminders map[string]string) error {
	if len(reminders) == 0 {
		return errors.New("no reminders")
	}
	for name, reminder := range reminders {
		fmt.Printf("\t[%s] \t%s\n", name, reminder)
	}
	return nil
}

func getReminders(file *os.File) (map[string]string, error) {
	byteFile, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var reminders map[string]string
	err = json.Unmarshal(byteFile, &reminders)
	if err != nil {
		reminders = make(map[string]string)
	}
	return reminders, nil
}

func save(file *os.File, reminders map[string]string) error {
	byteFile, err := json.MarshalIndent(reminders, "", "\t")
	if err != nil {
		return err
	}
	file.Truncate(0)
	file.Seek(0, 0)
	_, err = file.Write(byteFile)
	return err
}
